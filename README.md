# sonatype

1. to run the software, please make sure that you have a JRE or JDK installed. Please note that JRE or JDK has to be at least version 1.7 for the software to run correctly.
2. if you don't have a JRE installed, you could download it from https://docs.oracle.com/goldengate/1212/gg-winux/GDRAD/java.htm#BGBFJHAB (together with the JRE itself, the website contains detailed instructions on how to perform this action)
3. in the target directory, there is a file called numberConverter-0.0.1-SNAPSHOT.jar. Navigate to this directory
4. using the command line, run the following command: java -jar numberConverter-0.0.1-SNAPSHOT.jar <input number>, where <input number> is the number you want the English word equivalent for
5. output will be in the next line

6. if you wish to run the tests, it is best to download Eclipse (from https://www.eclipse.org/downloads/ ) or some other IDE, import the project, and run them from src/test/java/com/numberConverter/NumberConverterApplicationTests.java (please note that JDK is preferable to JRE for this)