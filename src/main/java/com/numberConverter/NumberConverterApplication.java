package com.numberConverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NumberConverterApplication {

	private static final int ONE_BILLION = 1000000000;
	private static final int ONE_MILLION = 1000000;
	private static final int ONE_THOUSAND = 1000;
	private static final int ONE_HUNDRED = 100;
	private static final int TWENTY = 20;
	private static final int TEN = 10;
	private static final int ZERO = 0;
	
	//words used throughout the code
	private static final String[] ONES= {"","one","two","three","four","five","six","seven","eight","nine"};
	private static final String[] TEENS= {"ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"};
	private static final String[] TENS= {"","","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"};
	
	public static final String CONVERSION_FAILED = "The input value is not a valid number. Please enter a whole number value between -2147483648 and 2147483647";
	
	public static void main(String[] args) {
		SpringApplication.run(NumberConverterApplication.class, args);
		
		if(args.length == 0) {
			System.out.println(CONVERSION_FAILED);
		}
		else {			
			System.out.println(convert(args[0]));
		}
	}

	public static String convert(String inputString) {
		StringBuffer sb = new StringBuffer();
		
		long numberToBeConverted = 0;
		try {
			boolean shouldContainAnd = false;
			numberToBeConverted = Long.parseLong(inputString);
			
			//we are working with longs, because INTEGER.MIN_VALUE can't be multiplied by -1, and this is something our code relies on for negative numbers
			//check that the input can be parse as an int, to restrict values outside of integer range
			//if the parse passes, this means we should move on with the conversion (and we're still converting the long)
			//if the parse fails, we are outside of given range, and an exception will be thrown
			int numberToBeConvertedInt = Integer.parseInt(inputString);
			
			//shouldContainAnd is telling us if we need to add 'and' to the output, before the text of the last two digits
			if ((numberToBeConverted >= ONE_HUNDRED) && (numberToBeConverted % ONE_HUNDRED != 0)){
				shouldContainAnd = true;
			}
			
			//if the value is zero, just print zero
			if (numberToBeConverted == ZERO){
				sb.append("zero");
			} 
			//otherwise, run the conversion
			else{
				//if we are converting a negative value, append the word 'minus' to the start of the string
				//multiply the number by -1, and continue conversion as if we received a positive value
				if (numberToBeConverted < 0){
					sb.append("minus ");
					numberToBeConverted *= -1;
				}
				sb.append(convertBillions(numberToBeConverted, shouldContainAnd));
			}

		}
		catch (NumberFormatException nfe) {
			return CONVERSION_FAILED;
		}
		
		return capitalizeFirst(removeAllButLastAnd(sb.toString().trim()));
	}
	
	private static StringBuffer convertBillions(long numberToBeConverted, boolean shouldContainAnd) {
		StringBuffer finalBillionsWords = new StringBuffer();
		//check if the number is greater than one billion
		if (numberToBeConverted >= ONE_BILLION){
			finalBillionsWords = convertBillions((int) Math.floor(numberToBeConverted / ONE_BILLION), shouldContainAnd).append(" billion ").append(convertMillions(numberToBeConverted % ONE_BILLION, shouldContainAnd));
		}
		else {
			finalBillionsWords = convertMillions(numberToBeConverted, shouldContainAnd);
		}

		return finalBillionsWords;
	}
	
	private static StringBuffer convertMillions(long numberToBeConverted, boolean shouldContainAnd){
		StringBuffer finalMillionsWords = new StringBuffer();
		//check if the number is greater than one million
		if (numberToBeConverted >= ONE_MILLION){
			finalMillionsWords = convertMillions((int) Math.floor(numberToBeConverted / ONE_MILLION), shouldContainAnd).append(" million ").append(convertThousands(numberToBeConverted % ONE_MILLION, shouldContainAnd));
		}
		else {
			finalMillionsWords = convertThousands(numberToBeConverted, shouldContainAnd);
		}

		return finalMillionsWords;
	}
	
	private static StringBuffer convertThousands(long numberToBeConverted, boolean shouldContainAnd){
		StringBuffer finalThousandsWords = new StringBuffer();
		//check if the number is greater than one thousand
		if (numberToBeConverted >= ONE_THOUSAND){
			finalThousandsWords = convertHundreds((int)Math.floor(numberToBeConverted / ONE_THOUSAND), shouldContainAnd).append(" thousand ").append(convertHundreds(numberToBeConverted % ONE_THOUSAND, shouldContainAnd));
		}
		else{
			finalThousandsWords = convertHundreds(numberToBeConverted, shouldContainAnd);
		}
		
		return finalThousandsWords;
	}
	
	private static StringBuffer convertHundreds(long numberToBeConverted, boolean shouldContainAnd){
		StringBuffer finalHundredsWords = new StringBuffer();
		StringBuffer tensWords = convertTens(numberToBeConverted % ONE_HUNDRED);
		//check if the number is greater than one hundred
		if (numberToBeConverted >= ONE_HUNDRED){
			StringBuffer hundredsWords = new StringBuffer(ONES[(int)Math.floor(numberToBeConverted / ONE_HUNDRED)]).append(" hundred");
			//check the last two digits of the number
			if (tensWords.length() > 0){
				//and is capital here, because we ALWAYS want to have 'and' in such cases
				finalHundredsWords = hundredsWords.append(" AND ").append(tensWords);
			}
			else {
				finalHundredsWords = hundredsWords;
			}
		}
		else{
			//this approach could result in false positive and's dangling in the output, they will be removed in the final steps
			if (shouldContainAnd){
				finalHundredsWords.append("and ");
			}
			finalHundredsWords.append(tensWords);
		}
		
		return finalHundredsWords;
	}
	
	private static StringBuffer convertTens(long numberToBeConverted){
		StringBuffer finalTensWords;
		//check if the number is lower than ten, between ten and twenty, or larger than twenty
		if (numberToBeConverted < TEN){
			finalTensWords = new StringBuffer(ONES[(int) numberToBeConverted]);
		}
		else if (numberToBeConverted >= TEN && numberToBeConverted < TWENTY){
			finalTensWords = new StringBuffer(TEENS[(int) (numberToBeConverted - TEN)]);
		} 
		else {
			finalTensWords = new StringBuffer(TENS[(int) Math.floor(numberToBeConverted / TEN)] + ' ' + ONES[(int) (numberToBeConverted % TEN)]);
		}

		return finalTensWords;
	}
	
	//method that removes leading 'and', it should only appear for tens, not tens of thousands etc
	private static String removeAllButLastAnd(String words){
		//if the output starts with 'and', this is definitely a mistake, remove it
		if (words.indexOf("and ") == 0){
			words = words.substring(4);
		}
		if (words.indexOf(" and " ) > 0){
			int lastAnd = words.lastIndexOf(" and ");
			String allButLast = words.substring(0, lastAnd).replaceAll(" and ", " ");
			return allButLast + words.substring(lastAnd);
		}
		else{
			//we convert the word to lower case, because of capital AND's
			//the first letter will be capitalized in the final step
			return words.toLowerCase();
		}
	}

	//method that capitalizes the first letter of the input string
	private static String capitalizeFirst(String words){
		return words.substring(0, 1).toUpperCase() + words.substring(1);
	}

}
