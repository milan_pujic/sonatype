package com.numberConverter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NumberConverterApplicationTests {

	@Test
	public void contextLoads() {
	}
	
	@Test 
	public void sample() {		
		assertEquals("Zero", NumberConverterApplication.convert("0") );
		assertEquals("Thirteen", NumberConverterApplication.convert("13") );
		assertEquals("Eighty five", NumberConverterApplication.convert("85") );
		assertEquals("Five thousand two hundred and thirty seven", NumberConverterApplication.convert("5237") );
	}
	
	@Test
	public void incorrectInput() {
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert(""));
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert("some text"));
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert("15.45"));
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert("25-525+454"));
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert("66,976"));
	}
	
	@Test
	public void minAndMaxValues() {
		assertEquals("Two billion one hundred and forty seven million four hundred and eighty three thousand six hundred and forty seven", NumberConverterApplication.convert(Integer.MAX_VALUE + "") );
		assertEquals("Minus two billion one hundred and forty seven million four hundred and eighty three thousand six hundred and forty eight", NumberConverterApplication.convert(Integer.MIN_VALUE + "") );
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert("2147483648") );
		assertEquals(NumberConverterApplication.CONVERSION_FAILED, NumberConverterApplication.convert("-2147483649") );
	}
	
	@Test
	public void simpleTest(){
		assertEquals("Seventeen", NumberConverterApplication.convert("17") );
		assertEquals("Thirty four", NumberConverterApplication.convert("34") );
		assertEquals("One hundred and thirty four", NumberConverterApplication.convert("134") );
	}
	
	@Test
	public void severalZeroesInARow() {
		assertEquals("Five million three hundred thousand and one", NumberConverterApplication.convert("5300001") );
		assertEquals("Fifty three million seven hundred and eighty seven thousand", NumberConverterApplication.convert("53787000") );
		assertEquals("Two", NumberConverterApplication.convert("0002") );
		assertEquals("Three million ten thousand", NumberConverterApplication.convert("0003010000") );
	}

	@Test
	public void mixOfFalsePositiveAndRealAnds() {
		assertEquals("Six million three thousand and three", NumberConverterApplication.convert("6003003") );
	}
}

